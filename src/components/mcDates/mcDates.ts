///<reference path="../../typings.d.ts"/>
import template from './mcDates.html';
import './mcDates.less';

import * as moment from 'moment';

const DATE_FORMAT = 'YYYY-MM-DD';

enum PredefinedDatesRange {
  Yesterday, Today, TwoWeeks, Month, All
};

class McDatesController {
  PredefinedDatesRange = PredefinedDatesRange;

  from: Date = null;
  to: Date = null;

  minDate: Date = null;
  maxDate: Date = null;

  private _origFrom: String = null;
  private _origTo: String = null;

  mcChange: () => {};

  constructor(public $timeout: any) {}

  set dateFrom(value: String) {
    this._origFrom = value;

    this.minDate = this.from = this._isDateStringValid(value) && (!this.to || moment(value).isSameOrBefore(this.to))
      ? moment(value).toDate()
      : null
  }

  set dateTo(value: String) {
    this._origTo = value;
    this.maxDate = this.to = this._isDateStringValid(value) && (!this.from || moment(value).isSameOrAfter(this.from))
      ? moment(value).toDate()
      : null
  }

  get dateFrom(): String {
    return this.from ? this._formatDateString(this.from) : this._origFrom || null;
  }

  get dateTo(): String {
    return this.to ? this._formatDateString(this.to) : this._origTo || null;
  }

  changeDate(): void {
    this._executeChangeCallback();
  }

  selectPredefinedDates(rangeType: PredefinedDatesRange): void {
    let oldFromDate: Date = this.from;
    let oldToDate: Date = this.to;

    // Вся это колдовство с maxDate, minDate и $timeout
    // нужно, чтобы не подсвечивалась валидация на датапикере
    this.maxDate = this.minDate = null;
    
    this.$timeout(() => {
      switch (rangeType) {
        case PredefinedDatesRange.Yesterday:
          this.from = this.to = moment(new Date()).add(-1, 'd').toDate();
          break;
        case PredefinedDatesRange.Today:
          this.from = this.to = moment(new Date()).toDate();
          break;
        case PredefinedDatesRange.TwoWeeks:
          this.from = moment(new Date()).toDate();
          this.to = moment(new Date()).add(14, 'd').toDate();
          break;
        case PredefinedDatesRange.Month:
          this.from = moment(new Date()).toDate();
          this.to = moment(new Date()).add(1, 'M').toDate()
          break;
        case PredefinedDatesRange.All:
        default:
          this.from = this.to = null;
          break;
      }

      this.minDate = this.from;
      this.maxDate = this.to;
  
      this._origFrom = this.from ? this._formatDateString(this.from) : null;
      this._origTo = this.to ? this._formatDateString(this.to) : null;

      if (!moment(oldFromDate).isSame(this.from, 'day') || !moment(oldToDate).isSame(this.to, 'day')) {
        this._executeChangeCallback();    
      }
    });

  }

  private _isDateStringValid(dateString: String): Boolean {
    return moment(dateString, DATE_FORMAT).format(DATE_FORMAT) === dateString;
  }

  private _formatDateString(dateString: Date): String {
    return moment(dateString).format(DATE_FORMAT);
  }

  private _executeChangeCallback(): void {
    this.mcChange && this.$timeout(this.mcChange);
  }
}

McDatesController.$inject = ['$timeout']

export default {
  bindings: {
    'dateFrom': '=',
    'dateTo': '=',
    'mcChange': '<'
  },
  template,
  controller: McDatesController
}