import * as angular from 'angular';
import * as moment from 'moment';

import 'angular-animate';
import 'angular-messages';
import 'angular-aria';

import 'angular-material';
import 'angular-material/angular-material.css';

import maDatesDefinition from './components/mcDates/mcDates';

const DATE_FORMAT = 'DD.MM.YYYY';

const testApp = angular.module("testApp", ['ngMaterial', 'ngMessages']);

testApp
  .config(function ($mdDateLocaleProvider: any) {
    $mdDateLocaleProvider.formatDate = function (date: Date): string {
      return date ? moment(date).format(DATE_FORMAT) : '';
    };

    $mdDateLocaleProvider.parseDate = function (dateString: string): Date {
      let m = moment(dateString, DATE_FORMAT, true);
      return m.isValid() ? m.toDate() : new Date(NaN);
    };

    $mdDateLocaleProvider.isDateComplete = function (dateString: string): boolean {
      dateString = dateString.trim();
      let re = /^(([a-zA-Z]{3,}|[0-9]{1,4})([ .,]+|[/-]))([a-zA-Z]{3,}|[0-9]{1,4})/;
      return re.test(dateString);
    };
  })
  .controller("mainController", function () {
    this.date1="1992-12-22";
    this.date2="1992-12-23";

    this.changeDates = () => {
      alert(`с: ${this.date1}   по: ${this.date2}`);
    };
  })
  .component('mcDates', maDatesDefinition);